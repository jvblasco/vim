" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
"if has("autocmd")
"  filetype plugin indent on
"endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
"set showmatch		" Show matching brackets.
"set ignorecase		" Do case insensitive matching
"set smartcase		" Do smart case matching
"set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden             " Hide buffers when they are abandoned
"set mouse=a		" Enable mouse usage (all modes)

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

"General custom configs

"Vim powerline fancy symbols
let g:Powerline_symbols = 'fancy'
"set encoding=utf-8

" Format the status line
"set statusline=%{fugitive#statusline()}\ [Format=%{&ff}]\ [Type=%Y]\ [Pos=%04l,%04v][%p%%]\ [LEN=%L] 
" Format the status line
"set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l
" Always show the status line
set laststatus=2

"Show line numbers
set number

"Ignore compiled files
set wildignore=*.o,*~,*.pyc

"Python IDE configuration sart

"Pathogen config
filetype off
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

"Code folding enabling
"set foldmethod=indent
"set foldlevel=99

"Indentation
set tabstop=4
set shiftwidth=4
set smarttab

"Splited window navigation using only Ctrl + movement keys
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

"Task list mapping
map <leader>td <Plug>TaskList

"Gundo Toggle to view diffs between saves
map <leader>g :GundoToggle<CR>

"Syntax highlighting enable
syntax on                           " syntax highlighing
filetype on                          " try to detect filetypes
filetype plugin indent on    " enable loading indent file for filetype

"Tell pyflakes-pathogen submodule to avoid using quickfix window
let g:pyflakes_use_quickfix = 0

"Jump between each pep8 violations in the quickfix window
let g:pep8_map='<leader>8'

"SuperTab context sensitivity configuration
au FileType python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType = "context"
"Pydoc preview enabling
set completeopt=menuone,longest,preview

"NERD Tree toogling map
map <leader>n :NERDTreeToggle<CR>

"Custom mappings for insert mode
map! ,pr System.out.println("");<ESC>2hi
map! ,pm System.out.println("Soy el método de la clase");<ESC>4bi
map! ,pc System.out.println("Soy el contructor de la clase ");<ESC>2hi
map! ,fi <field name=""></field><ESC>9hi
map! ,sfi <field name=""/><ESC>2hi
