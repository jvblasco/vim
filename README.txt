To make every plugin work we need to install some packages and compile command-t.

To do that we first need to install a C compiler (it should come included in the build-essential package):
$ sudo aptitude install ruby ruby-dev

After that we will need to install the ruby and ruby-dev packages. Keep in mind that to make the plugin command-t work
we will need vim compiled with ruby support. The vim-nox package comes compiled with support for ruby and other scripting
languages as well:
$ sudo aptitude remove vim && sudo aptitude install vim-nox

We will navigate to ~/.vim/bundle/command-t/ruby/command-t and execute:
$ ruby extconf.rb 
$ make
